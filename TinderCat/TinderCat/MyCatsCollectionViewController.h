//
//  MyCatsCollectionViewController.h
//  TinderCats
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <UIKit/UIKit.h>
@import CoreData;
#import "CatCollectionViewCell.h"

@interface MyCatsCollectionViewController : UICollectionViewController <NSFetchedResultsControllerDelegate, CatCollectioViewCellDelegate>

// Core data ivars
@property (strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController;

@end
