//
//  CatCollectionViewCell.m
//  TinderCats
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "CatCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"

@implementation CatCollectionViewCell

- (void)prepareForReuse {
    // Required by AFNetworking+UIImageView
    [self.catImage cancelImageDownloadTask];
}

- (void)layoutSubviews {
     self.deleteButton.layer.cornerRadius = 15.0;
}

// Menu callback
- (void)delete:(id)sender {
    if([self.delegate respondsToSelector:@selector(deleteHasBeenPressed:forCell:)]) {
        [self.delegate deleteHasBeenPressed:sender forCell:self];
    }
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    return (action == NSSelectorFromString(@"delete:"));
}

@end
