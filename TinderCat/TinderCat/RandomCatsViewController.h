//
//  RandomCatsViewController.h
//  TinderCats
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <UIKit/UIKit.h>
#import "CatApiHelper.h"

@interface RandomCatsViewController : UIViewController <UIGestureRecognizerDelegate>

// Hate/Love Buttons outlets
@property (weak) IBOutlet UIButton* hateButton;
@property (weak) IBOutlet UIButton* loveButton;
@property (weak) IBOutlet UISwipeGestureRecognizer* swipeLeft;
@property (weak) IBOutlet UISwipeGestureRecognizer* swipeRight;

// Image Views (old one is on screen, new one is preloaded and will appear)
@property (strong) UIImageView* catNewImageView;
@property (strong) UIImageView* catOldImageView;

// Horizontal position constraint of shown image view
@property (strong) NSLayoutConstraint* catImagePositionConstraint;

// Core data context
@property (strong) NSManagedObjectContext* managedContext;

// Api helper
@property (strong) CatApiHelper* catApiHelper;

// Button actions
- (IBAction)discardTheCat:(id)sender;
- (IBAction)saveTheCat:(id)sender;


@end
