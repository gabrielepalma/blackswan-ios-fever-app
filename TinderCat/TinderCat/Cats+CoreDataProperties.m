//
//  Cats+CoreDataProperties.m
//  TinderCats
//
//  Created by Gabriele Palma on 04/05/16.
//  Copyright © 2016 Gabriele Palma. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Cats+CoreDataProperties.h"

@implementation Cats (CoreDataProperties)

@dynamic address;

@end
