//
//  RandomCatsViewController.m
//  TinderCats
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "RandomCatsViewController.h"
#import "AppDelegate.h"
#import "Cats.h"
#import "UIImageView+AFNetworking.h"

typedef enum TinderCatAnimationSide {
    LeftSide,
    RightSide
} TinderCatAnimationSideEnum;

@interface RandomCatsViewController ()

// Is an animation in progress?
@property BOOL isAnimating;

@end

@implementation RandomCatsViewController

// Constraint constants for image view position
static const int MARGINTOP = 0;
static const int MARGINBOTTOM = 16;
static const int MARGINSIDES = 10;

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
       AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        self.managedContext = [appDelegate managedObjectContext];
        self.catApiHelper = [CatApiHelper new];
        self.catApiHelper.elementsPerRequest = 3;
        _isAnimating = false;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.hateButton.layer.cornerRadius = 15.0;
    self.loveButton.layer.cornerRadius = 15.0;
    [self setupIdleStateForAnimations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark User interactions

// I animate the image views from left to right. If current url is valid and not an error i also save it in core data
- (IBAction)saveTheCat:(id)sender {
    if(!_isAnimating && [self.catApiHelper hasCurrentCat]){
        NSString* address = [self.catApiHelper retrieveCurrentCat];
        _isAnimating = true;
        if(![address isEqualToString:@""]){
            NSEntityDescription* desc = [NSEntityDescription entityForName:@"Cats" inManagedObjectContext:self.managedContext];
            Cats* newCat = [[Cats alloc] initWithEntity:desc insertIntoManagedObjectContext:self.managedContext];
            newCat.address = address;
            [self.managedContext save:nil];
        }
        [self.catApiHelper consumeCurrentCat];
        [self animateTransitionFromSide:LeftSide];
        
    }
}

// I animate the miage views from right to left. If current url is valid and not an error i also discard it
- (IBAction)discardTheCat:(id)sender {
    if(!_isAnimating  && [self.catApiHelper hasCurrentCat]){
        _isAnimating = true;
        [self.catApiHelper consumeCurrentCat];
        [self animateTransitionFromSide:RightSide];
        
    }
}

// I call the above methods accordingly.
-(IBAction)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    if(recognizer.direction == UISwipeGestureRecognizerDirectionLeft){
        [self discardTheCat:nil];
    }
    else {
        [self saveTheCat:nil];
    }
    
}

#pragma mark Networking

// Load a cat on an image view. Alternative images are loaded as placeholders or in case of any type of errors
- (void) loadRandomCatOnImageView:(UIImageView*)imageView withCompletionHandler:(void(^)()) handler {
    [self.catApiHelper fetchRandomCatUrlAndExecute:^(NSString *url)  {
        if(url){
            NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]
                                                          cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                      timeoutInterval:60];
            
            imageView.image = [UIImage imageNamed:@"placeholder"];
            [imageView setImageWithURLRequest:imageRequest
                             placeholderImage:[UIImage imageNamed:@"placeholder"]
                                      success:nil
                                      failure:nil];
        }
        else {
            imageView.image = [UIImage imageNamed:@"placeholder"];
        }
        if(handler) handler();
    } onFailure:^(NSError * error) {
        imageView.image = [UIImage imageNamed:@"error"];
        if(handler) handler();
    }];
}

#pragma mark Layout animation

// The swipe animation is done using autolayouts animations
// On idle state there's an image view visible on screen and an image view off the scene, both loaded with a cat
// When animating a change the autolayouts are updated accordingly either to animate towards the left or right
// and at the end of the animation the state is brought back to match the idle state
// When animations are running, further commands are ignored

// Idle state configuration on load
- (void) setupIdleStateForAnimations {
    self.catOldImageView = [UIImageView new];
    self.catOldImageView.contentMode = UIViewContentModeScaleAspectFit;

    self.catNewImageView = [UIImageView new];
    self.catNewImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self loadRandomCatOnImageView:self.catOldImageView withCompletionHandler:^{
        [self loadRandomCatOnImageView:self.catNewImageView withCompletionHandler:nil];
    }];
    
    [self.view addSubview:self.catOldImageView];
    [self setupDefaultConstraintFor:self.catOldImageView];
    self.catImagePositionConstraint = [self getCenterConstraintForImageView:self.catOldImageView];
    [self.view addConstraint:self.catImagePositionConstraint];
}

// Setup all constraints except position
- (void) setupDefaultConstraintFor:(UIImageView*)imageView {
    [imageView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:-MARGINSIDES*2]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.hateButton attribute:NSLayoutAttributeTop multiplier:1.0 constant:-MARGINBOTTOM]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTopMargin multiplier:1.0 constant:MARGINTOP]];
}

// Convenience method to get center positional contraint
- (NSLayoutConstraint*) getCenterConstraintForImageView:(UIImageView*)imageView {
    return [NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
}

// Convenience method to get left/right positional contraint
- (NSLayoutConstraint*) getConstraintForImageView:(UIImageView*)imageView fromSide:(TinderCatAnimationSideEnum)side{
    if(side==LeftSide)  return [NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0];
    else                return [NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0 constant:0];
}

// Perform the animation (from left to right, or from right to left) and returns the view controller to a valid idle state at the end of it

- (void)animateTransitionFromSide:(TinderCatAnimationSideEnum)side {
    [self.view addSubview:(self.catNewImageView)];
    [self setupDefaultConstraintFor:self.catNewImageView];
    NSLayoutConstraint* positionalConstraint = [self getConstraintForImageView:self.catNewImageView fromSide:side];
    [self.view addConstraint:positionalConstraint];
    
    [self.view layoutIfNeeded];
    
    [self.view removeConstraint:self.catImagePositionConstraint];
    [self.view addConstraint:[self getConstraintForImageView:self.catOldImageView fromSide:(side==LeftSide)?RightSide:LeftSide]];
    [self.view removeConstraint:positionalConstraint];
    [self.view addConstraint:self.catImagePositionConstraint = [self getCenterConstraintForImageView:self.catNewImageView]];
    
    [UIView animateWithDuration:0.4 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.catOldImageView removeFromSuperview];
        self.catOldImageView = self.catNewImageView;
        self.catNewImageView = [UIImageView new];
        self.catNewImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self loadRandomCatOnImageView:self.catNewImageView withCompletionHandler:nil];
        
        _isAnimating = false;
    }];
}

@end
