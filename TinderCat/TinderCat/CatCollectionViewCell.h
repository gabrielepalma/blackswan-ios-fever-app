//
//  CatCollectionViewCell.h
//  TinderCats
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <UIKit/UIKit.h>

@class CatCollectionViewCell;

@protocol CatCollectioViewCellDelegate <NSObject>
- (void)deleteHasBeenPressed:(id)sender forCell:cell;
@end

@interface CatCollectionViewCell : UICollectionViewCell

@property (weak) IBOutlet UIImageView* catImage;
@property (weak) IBOutlet UIButton* deleteButton;
@property (weak) id<CatCollectioViewCellDelegate> delegate;

@end
