//
//  Cats+CoreDataProperties.h
//  TinderCats
//
//  Created by Gabriele Palma on 04/05/16.
//  Copyright © 2016 Gabriele Palma. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Cats.h"

NS_ASSUME_NONNULL_BEGIN

@interface Cats (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *address;

@end

NS_ASSUME_NONNULL_END
