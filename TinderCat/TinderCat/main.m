//
//  main.m
//  TinderCats
//
//  Created by Gabriele Palma on 04/05/16.
//  Copyright © 2016 Gabriele Palma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
