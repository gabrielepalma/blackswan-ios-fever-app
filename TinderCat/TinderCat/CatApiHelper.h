//
//  CatApiHelper.h
//  TinderCats
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <Foundation/Foundation.h>
#import "CatApiModel.h"

@interface CatApiHelper : NSObject

// Minimum 2 elements for requests, maximum 100
@property int elementsPerRequest;

// Cats that have been fetched and not consumed
@property NSMutableArray<CatImageModel*>* catsArray;

// Index for next random cat
@property int nextCatIndex;

// Api key and base url. By default are fetched from plist
@property NSString* apiKey;
@property NSString* apiUrl;

- (void) fetchRandomCatUrlAndExecute:(void(^)(NSString* url))blockSucces onFailure:(void(^)(NSError*))blockFailure;
- (void) consumeCurrentCat;
- (NSString*) retrieveCurrentCat;
- (BOOL) hasCurrentCat; 

@end
