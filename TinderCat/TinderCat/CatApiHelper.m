//
//  CatApiHelper.m
//  TinderCats
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "CatApiHelper.h"
#import "CatApiModel.h"
#import <AFHTTPSessionManager.h>
#import "XMLDictionary.h"

@interface CatApiHelper ()

@end

@implementation CatApiHelper

- (instancetype)init {
    self = [super init];
    if (self) {
        _nextCatIndex = 0;
        _catsArray = [NSMutableArray new];
        self.apiUrl = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CatBaseUrl"];
        self.apiKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CatApiKey"];
    }
    return self;
}

// Hello recruiter, I know there are easier ways to fetch an url and parse a simple XML using basic iOS SDK.
// Still, I used AFNetworking and serialized OOP models, which indeed seems a bit verbose and articulate in this case, because that's how i would solve a real world case with a more complex data model

// It fetches an url from the API: if one is already available it uses previously fetched urls, otherwise it initiate a call to the API to get a new list and cache it
- (void) fetchRandomCatUrlAndExecute:(void(^)(NSString* url))blockSucces onFailure:(void(^)(NSError* error))blockFailure {
    static int fetched = 0;
    
    if(_nextCatIndex < [_catsArray count]){
        blockSucces([_catsArray objectAtIndex:_nextCatIndex].url);
        _nextCatIndex++;
    }
    else {
        NSURL *baseURL = [NSURL URLWithString:self.apiUrl];
        NSDictionary *parameters = @{@"results_per_page": [NSNumber numberWithInt:self.elementsPerRequest], @"format": @"xml",  @"api_key": self.apiKey };
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/xml"];
        [manager GET:@"images/get" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseData) {
            XMLDictionaryParser* dictionary = [XMLDictionaryParser new];
            NSDictionary* responseObject = [dictionary dictionaryWithData:responseData];
            responseObject = [responseObject objectForKey:@"data"]; // skip unused entry
            responseObject = [responseObject objectForKey:@"images"]; // skip unused entry
            CatApiModel* list = [[CatApiModel alloc] initWithDictionary:responseObject  error:nil];
            if([list.image count] == 0){
                NSLog(@"Unresolved error from API: API returned no cats!");
                // The errorCat is needed to maintain consistency with consume and retrieve methods
                CatImageModel* errorCat = [CatImageModel new]; errorCat.url = @"";
                [self.catsArray insertObject:errorCat atIndex:_nextCatIndex];
                _nextCatIndex++;fetched++;
                blockFailure(nil);
            }
            else {
                [_catsArray addObjectsFromArray:list.image];
                _nextCatIndex++;fetched+=list.image.count;
                blockSucces([_catsArray objectAtIndex:_nextCatIndex-1].url);
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"Unresolved error from API: %@", error);
            // The errorCat is needed to maintain consistency with consume and retrieve methods
            CatImageModel* errorCat = [CatImageModel new]; errorCat.url = @"";
            [self.catsArray insertObject:errorCat atIndex:_nextCatIndex];
            _nextCatIndex++;fetched++;
            blockFailure(error);
        }];
    }
}

// Functions to consume and retrieve previously returned random image URLs
// Both methods assumes fetchRandomCatUrlAndExecute has been called first, once for each consumed url: it might fail otherwise (and we want it to crash if it does)
- (void) consumeCurrentCat {
    static int consumed = 0;
    NSLog(@"consuming %d %d", consumed, _catsArray.count);
    consumed++;
    [_catsArray removeObjectAtIndex:0];
    _nextCatIndex--;
}
- (NSString*) retrieveCurrentCat {
    return [_catsArray objectAtIndex:0].url;
}
- (BOOL) hasCurrentCat {
    return (_catsArray.count > 0);
}
@end
