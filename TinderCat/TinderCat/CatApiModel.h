//
//  CatApiModel.h
//  TinderCats
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

// Required by JSONModel library, see docs
@protocol CatImageModel
@end

@interface CatImageModel : JSONModel
@property (strong) NSString* url;
@end


@interface CatApiModel : JSONModel
@property (strong) NSArray<CatImageModel>* image;
@end
