//
//  CatApiModelTest.m
//  TinderCats
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <XCTest/XCTest.h>
#import "CatApiHelper.h"
#import "CatApiModel.h"

@interface CatApiModelTest : XCTestCase

@property BOOL asynchronousIsDone;
@property CatApiHelper* unitUnderTest;

@end

@implementation CatApiModelTest

- (void)setUp {
    [super setUp];
     self.unitUnderTest = [CatApiHelper new];
    _asynchronousIsDone = 0;
    
}

- (void)tearDown {
    [super tearDown];
}

- (BOOL)waitForCompletion:(NSTimeInterval)timeoutSecs {
    NSDate *timeoutDate = [NSDate dateWithTimeIntervalSinceNow:timeoutSecs];
    do {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:timeoutDate];
        if([timeoutDate timeIntervalSinceNow] < 0.0)
            break;
    } while (_asynchronousIsDone == 0);
    return _asynchronousIsDone;
}

- (void)testApiFetch {
     _unitUnderTest.elementsPerRequest = 5;
    _unitUnderTest.catsArray = [NSMutableArray<CatImageModel*> new];
    
    __block NSString* returnedCat = nil;
    [_unitUnderTest fetchRandomCatUrlAndExecute:^(NSString *url) {
        _asynchronousIsDone = 1;
        returnedCat = url;
    } onFailure:^(NSError * error) {
        _asynchronousIsDone = 2;
    }];
    
    XCTAssertNotEqual([self waitForCompletion:10], 0);
    
    XCTAssertNotEqual([_unitUnderTest.catsArray objectAtIndex:0], nil);
    XCTAssertNotEqual([_unitUnderTest.catsArray objectAtIndex:1], nil);
    XCTAssertNotEqual([_unitUnderTest.catsArray objectAtIndex:2], nil);
    XCTAssertNotEqual([_unitUnderTest.catsArray objectAtIndex:3], nil);
    XCTAssertNotEqual([_unitUnderTest.catsArray objectAtIndex:4], nil);
    XCTAssertNotEqual([_unitUnderTest.catsArray objectAtIndex:0].url, nil);
    XCTAssertNotEqual([_unitUnderTest.catsArray objectAtIndex:1].url, nil);
    XCTAssertNotEqual([_unitUnderTest.catsArray objectAtIndex:2].url, nil);
    XCTAssertNotEqual([_unitUnderTest.catsArray objectAtIndex:3].url, nil);
    XCTAssertNotEqual([_unitUnderTest.catsArray objectAtIndex:4].url, nil);
    XCTAssertNotEqual([_unitUnderTest.catsArray objectAtIndex:0].url, @"");
    XCTAssertNotEqual([_unitUnderTest.catsArray objectAtIndex:1].url, @"");
    XCTAssertNotEqual([_unitUnderTest.catsArray objectAtIndex:2].url, @"");
    XCTAssertNotEqual([_unitUnderTest.catsArray objectAtIndex:3].url, @"");
    XCTAssertNotEqual([_unitUnderTest.catsArray objectAtIndex:4].url, @"");
    // n.b. If needed, more thourough testing could be achieved by mocking network interaction thru direct NSURLProtocol injection
    // i.e. see http://www.infinite-loop.dk/blog/2011/09/using-nsurlprotocol-for-injecting-test-data/

    XCTAssertEqual(_unitUnderTest.catsArray.count, 5);
    XCTAssertEqual(_unitUnderTest.nextCatIndex, 1);
    XCTAssertEqual(_asynchronousIsDone, 1);
}

- (void)testApiFetchFail {
    _unitUnderTest.elementsPerRequest = 0; // forces an error
    _unitUnderTest.catsArray = [NSMutableArray<CatImageModel*> new];
    
    [_unitUnderTest fetchRandomCatUrlAndExecute:^(NSString *url) {
        _asynchronousIsDone = 1;
    } onFailure:^(NSError * error) {
        _asynchronousIsDone = 2;
    }];
    
    XCTAssertNotEqual([self waitForCompletion:10], 0);
    XCTAssertEqual(_unitUnderTest.catsArray.count, 1);
    XCTAssertEqual(_unitUnderTest.nextCatIndex, 1);
    XCTAssertEqual(_asynchronousIsDone, 2);
}

- (void)testCacheFetch {
    CatImageModel* cat1 = [CatImageModel new]; cat1.url = @"cat1url";
    CatImageModel* cat2 = [CatImageModel new]; cat2.url = @"cat2url";
    CatImageModel* cat3 = [CatImageModel new]; cat3.url = @"cat3url";
    CatImageModel* cat4 = [CatImageModel new]; cat4.url = @"cat4url";
    _unitUnderTest.catsArray = [[NSMutableArray alloc] initWithArray:@[cat1, cat2, cat3, cat4]];
    _unitUnderTest.nextCatIndex = 1;
    
    __block NSString* returnedCat = nil;
    [_unitUnderTest fetchRandomCatUrlAndExecute:^(NSString *url) {
        returnedCat = url;
        _asynchronousIsDone = 1;
    } onFailure:^(NSError * error) {
        _asynchronousIsDone = 2;
    }];
    
    XCTAssertNotEqual([self waitForCompletion:10], 0);
    XCTAssertEqual(_unitUnderTest.catsArray.count, 4);
    XCTAssertEqual([_unitUnderTest.catsArray objectAtIndex:0].url, @"cat1url");
    XCTAssertEqual([_unitUnderTest.catsArray objectAtIndex:1].url, @"cat2url");
    XCTAssertEqual([_unitUnderTest.catsArray objectAtIndex:2].url, @"cat3url");
    XCTAssertEqual([_unitUnderTest.catsArray objectAtIndex:3].url, @"cat4url");
    XCTAssertEqual(returnedCat, @"cat2url");
    XCTAssertEqual(_unitUnderTest.nextCatIndex, 2);
    XCTAssertEqual(_asynchronousIsDone, 1);
}

- (void)testConsume {
    CatImageModel* cat1 = [CatImageModel new]; cat1.url = @"cat1url";
    CatImageModel* cat2 = [CatImageModel new]; cat2.url = @"cat2url";
    CatImageModel* cat3 = [CatImageModel new]; cat3.url = @"cat3url";
    CatImageModel* cat4 = [CatImageModel new]; cat4.url = @"cat4url";
    _unitUnderTest.catsArray = [[NSMutableArray alloc] initWithArray:@[cat1, cat2, cat3, cat4]];
    _unitUnderTest.nextCatIndex = 1;
    
    [_unitUnderTest consumeCurrentCat];
    
    XCTAssertEqual(_unitUnderTest.catsArray.count, 3);
    XCTAssertEqual([_unitUnderTest.catsArray objectAtIndex:0].url, @"cat2url");
    XCTAssertEqual([_unitUnderTest.catsArray objectAtIndex:1].url, @"cat3url");
    XCTAssertEqual([_unitUnderTest.catsArray objectAtIndex:2].url, @"cat4url");
    XCTAssertEqual(_unitUnderTest.nextCatIndex, 0);
}

- (void)testRetrieve {
    CatImageModel* cat1 = [CatImageModel new]; cat1.url = @"cat1url";
    CatImageModel* cat2 = [CatImageModel new]; cat2.url = @"cat2url";
    CatImageModel* cat3 = [CatImageModel new]; cat3.url = @"cat3url";
    CatImageModel* cat4 = [CatImageModel new]; cat4.url = @"cat4url";
    _unitUnderTest.catsArray = [[NSMutableArray alloc] initWithArray:@[cat1, cat2, cat3, cat4]];
    _unitUnderTest.nextCatIndex = 1;
    
    NSString* returnedCat = [_unitUnderTest retrieveCurrentCat];
    
    XCTAssertEqual(_unitUnderTest.catsArray.count, 4);
    XCTAssertEqual([_unitUnderTest.catsArray objectAtIndex:0].url, @"cat1url");
    XCTAssertEqual([_unitUnderTest.catsArray objectAtIndex:1].url, @"cat2url");
    XCTAssertEqual([_unitUnderTest.catsArray objectAtIndex:2].url, @"cat3url");
    XCTAssertEqual([_unitUnderTest.catsArray objectAtIndex:3].url, @"cat4url");
    XCTAssertEqual(returnedCat, @"cat1url");
    XCTAssertEqual(_unitUnderTest.nextCatIndex, 1);
}



@end
