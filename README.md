Breezometer is an app to display air quality information.
It has 3 view controllers: the first one allow the user to select a location from the map (long press on the MKMap) or select the current location using CLLocator; the second, available in the tab bar, can display in a table view the bookmarks saved in a CoreData database and finally the third (available from both previous ones) to retrieve and display air quality information of a location (again in a table view), by providing coordinates or a bookmark object. Some unit tests have been provided for all view controllers

TinderCat is an app to display random cats in a Tinder-like view controller. By swiping left or right or pressing the buttons the user can discard a cat or save it to the database. Saved cat are available in a second view controller, accessible from a button on the navigation bar, that shows the cat in a simple CollectionView. By using the typical collection view callback menu it’s possible to remove saved cats. Unit tests have been provided to test the api helper component that performs network queries, but not the view controllers.

For both apps it has been made use of AFNetworking and JSONModel to simplify networking and OOP encoding of json and xml responses. For some tests OCMock has been used to mock some object and for TinderCat a third party XML parser has been included to ease the XML to NSDictionary conversion. On Breezometer, especially for geocoder and geolocation features, Weakify and Strongify macros have been used to weaken self references in blocks.

# BlackSwan iOS Fever app

Show us what you can do and how clean your code is! Write an app that uses public APIs to fetch data and display it in an organized way.

Even if we all love playing with Swift (who doesn't ;)), please stick with the plain old Objective-C here.

For example:

* A StackOverflow client that shows the hottest questions
* An app that shows random images of cats from [The Cat API](http://thecatapi.com/)
* An app that downloads the NSHipster RSS feed showing and index of all the articles and that alerts the user when the latest article is published
* _Anything you can come up with!_

We could be sneaky and not say anything else, but here's some things we're looking to see:

* Use of existing open source libraries
* Standard best practises for Networking - Not reiventing the wheel
* Tests!
* **In this test** we don't care too much about the UI, what we care is how good the code is!

### Submission notes

You can just submit a PR here, create a private repo for free on [GitLab](https://www.gitlab.com/?gclid=CLCBmaWM474CFaMSwwodAqIAqw) or [Bitbucket](https://bitbucket.org/), or just send us the repo by email. Whatever you prefer.

---

[@BlackSwan](https://www.bkakswan.com) - 2014
