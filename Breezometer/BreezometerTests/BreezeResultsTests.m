//
//  BreezeResultsTests.m
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <XCTest/XCTest.h>
#import "BreezeResultsViewController.h"
#import "OCMock/OCMock.h"
#import "OCMock/OCMockObject.h"
#import "BreezometerApiMock.h"
#import "AppDelegate.h"


@interface BreezeResultsTests : XCTestCase

@property BreezeResultsViewController* vcUnderTest;

@end

@implementation BreezeResultsTests

- (void)setUp {
    [super setUp];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _vcUnderTest = [storyboard instantiateViewControllerWithIdentifier:@"breezeResultsViewController"];
    UIView* v = _vcUnderTest.view; v = v;
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"Bookmark"];
    NSError *error = nil;
    NSArray *results = [managedObjectContext executeFetchRequest:request error:&error];
    for (NSManagedObject *obj in results) {
        [managedObjectContext deleteObject:obj];
    }
    [managedObjectContext save:nil];
}

- (void)tearDown {
    _vcUnderTest = nil;
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"Bookmark"];
    NSError *error = nil;
    NSArray *results = [managedObjectContext executeFetchRequest:request error:&error];
    for (NSManagedObject *obj in results) {
        [managedObjectContext deleteObject:obj];
    }
    [managedObjectContext save:nil];
    [super tearDown];
}

- (void)testSuccessFromCoordinates {
    BreezometerApiMock* mock = [BreezometerApiMock new];

    NSDictionary* pollution = @{
                                   @"causes":@"causesText",
                                   @"effects":@"effectsText",
                                   @"main":@"mainText"
                                   };
    
    NSDictionary* hints = @{
                                  @"children":@"childrenText",
                                  @"health":@"childrenText",
                                  @"inside":@"insideText",
                                  @"outside":@"outsideText",
                                  @"sport":@"sportText"
                                  };
    NSDictionary* dict = @{
                                   @"breezometer_aqi":@"50",
                                   @"breezometer_description":@"descriptionText",
                                   @"random_recommendations":hints,
                                   @"dominant_pollutant_description":@"pollutantDescription",
                                   @"dominant_pollutant_text":pollution,
                                   };
    
    mock.error = nil;
    mock.responseObject = dict;
    _vcUnderTest.apiHelper = mock;
    
    [_vcUnderTest configureLocationAtLatitude:40.0 andLongitude:50.0 andCity:@"city"];
    [_vcUnderTest viewWillAppear:false];
    
    XCTAssertEqual([_vcUnderTest numberOfSectionsInTableView:_vcUnderTest.tableView], 3);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView numberOfRowsInSection:0], 3);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView numberOfRowsInSection:1], 5);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView numberOfRowsInSection:2], 4);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]].textLabel.text, @"city");
    NSString* str1 = [NSString stringWithFormat:@"%@", [_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]].textLabel.text];
    NSString* str2 = [NSString stringWithFormat:@"%@", [dict objectForKey:@"breezometer_aqi"]];
    XCTAssertEqual(str1, str2);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:2 inSection:0]].textLabel.text, [dict objectForKey:@"breezometer_description"]);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:1]].textLabel.text, [hints objectForKey:@"children"]);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:1]].textLabel.text, [hints objectForKey:@"health"]);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:2 inSection:1]].textLabel.text, [hints objectForKey:@"inside"]);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:3 inSection:1]].textLabel.text, [hints objectForKey:@"outside"]);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:4 inSection:1]].textLabel.text, [hints objectForKey:@"sport"]);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:2]].textLabel.text, [dict objectForKey:@"dominant_pollutant_description"]);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:2]].textLabel.text, [pollution objectForKey:@"main"]);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:2 inSection:2]].textLabel.text, [pollution objectForKey:@"effects"]);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:3 inSection:2]].textLabel.text, [pollution objectForKey:@"causes"]);
    XCTAssertEqual(_vcUnderTest.spinnerIndicator.hidden, true);
}

- (void) testConfigurationFromCoordinates {
    [_vcUnderTest configureLocationAtLatitude:40.0 andLongitude:50.0 andCity:@"city"];
    
    XCTAssertEqual(_vcUnderTest.bookmark, nil);
    XCTAssertEqualWithAccuracy(_vcUnderTest.latitude, 40.0, 1.0);
    XCTAssertEqualWithAccuracy(_vcUnderTest.longitude, 50.0, 1.0);
    XCTAssertEqual(_vcUnderTest.city, @"city");
}

- (void) testConfigurationFromObject {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Bookmark" inManagedObjectContext:managedObjectContext];
    Bookmark* bookmark = [[Bookmark alloc] initWithEntity:entity insertIntoManagedObjectContext:managedObjectContext];
    bookmark.latitude = [NSNumber numberWithFloat:40.0];
    bookmark.longitude = [NSNumber numberWithFloat:50.0];
    bookmark.city = @"city";
    [managedObjectContext insertObject:bookmark];
    [managedObjectContext save:nil];
    
    [_vcUnderTest configureLocationForBookmark:bookmark];
    
    XCTAssertEqual(_vcUnderTest.bookmark, bookmark);
    XCTAssertEqualWithAccuracy(_vcUnderTest.latitude, 40.0, 1.0);
    XCTAssertEqualWithAccuracy(_vcUnderTest.longitude, 50.0, 1.0);
    XCTAssertEqual(_vcUnderTest.city, @"city");
}

- (void)testEmpty {
    BreezometerApiMock* mock = [BreezometerApiMock new];
    NSDictionary* error = @{
                            @"code":@"21",
                            @"message":@"No data available for provided location",
                            };
    NSDictionary* dict = @{
                           @"data_valid":@"0",
                           @"error":error,
                           };
    mock.error = nil;
    mock.responseObject = dict;
    _vcUnderTest.apiHelper = mock;
    
    [_vcUnderTest configureLocationAtLatitude:40.0 andLongitude:50.0 andCity:@"city"];
    [_vcUnderTest viewWillAppear:false];
    
    XCTAssertEqual([_vcUnderTest numberOfSectionsInTableView:_vcUnderTest.tableView], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView numberOfRowsInSection:0], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]].textLabel.text, NSLocalizedString(@"BreezeResultsVC:EmptySet", nil));
    XCTAssertEqual(_vcUnderTest.spinnerIndicator.hidden, true);
}

- (void)testBookmarkDeletion {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Bookmark" inManagedObjectContext:managedObjectContext];
    Bookmark* bookmark = [[Bookmark alloc] initWithEntity:entity insertIntoManagedObjectContext:managedObjectContext];
    bookmark.latitude = [NSNumber numberWithFloat:40.0];
    bookmark.longitude = [NSNumber numberWithFloat:50.0];
    bookmark.city = @"city";
    [managedObjectContext insertObject:bookmark];
    [managedObjectContext save:nil];
    
    [_vcUnderTest configureLocationForBookmark:bookmark];
    [_vcUnderTest viewWillAppear:false];
    
    XCTAssertNotEqual(_vcUnderTest.bookmark, nil);
    
    [_vcUnderTest bookmarkTapped:_vcUnderTest.bookmarkButton];
    
    XCTAssertEqual(_vcUnderTest.bookmark, nil);
    
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"Bookmark"];
    NSError *error = nil;
    NSArray *results = [managedObjectContext executeFetchRequest:request error:&error];

    XCTAssertEqual([results indexOfObject:bookmark], NSNotFound);
    
}

- (void)testBookmarkInsertion {
    [_vcUnderTest configureLocationAtLatitude:40.0 andLongitude:50.0 andCity:@"city"];
    [_vcUnderTest viewWillAppear:false];
    
    XCTAssertEqual(_vcUnderTest.bookmark, nil);
    
    [_vcUnderTest bookmarkTapped:_vcUnderTest.bookmarkButton];

    XCTAssertNotEqual(_vcUnderTest.bookmark, nil);
    XCTAssertEqual(_vcUnderTest.city, @"city");
    XCTAssertEqualWithAccuracy(_vcUnderTest.latitude, 40.0, 1.0);
    XCTAssertEqualWithAccuracy(_vcUnderTest.longitude, 50.0, 1.0);
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *managedObjectContext = [appDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"Bookmark"];
    NSError *error = nil;
    NSArray *results = [managedObjectContext executeFetchRequest:request error:&error];
    
    XCTAssertNotEqual([results indexOfObject:_vcUnderTest.bookmark], NSNotFound);
}

@end
