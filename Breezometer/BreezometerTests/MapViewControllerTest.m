//
//  MapViewController.m
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <XCTest/XCTest.h>
#import "MapViewController.h"
#import "CLGeocoderMock.h"
#import "OCMock/OCMock.h"
#import "OCMock/OCMockObject.h"

@interface MapViewControllerTest : XCTestCase

@property MapViewController* vcUnderTest;

@end

@implementation MapViewControllerTest

- (void)setUp {
    [super setUp];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _vcUnderTest = [storyboard instantiateViewControllerWithIdentifier:@"mapViewController"];
    UIView* v = _vcUnderTest.view; v = v;
}

- (void)tearDown {
    _vcUnderTest = nil;
    [super tearDown];
}

- (void)testGeocoderSucceed {
    // this test is already covered by LocatorSucceeded
}
- (void)testGeocoderHangs {
    _vcUnderTest.geocoder = nil;
    
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(40.0, 50.0);
    [_vcUnderTest moveCurrentPositionToLocation:location];
    
    XCTAssertEqual(_vcUnderTest.mapView.annotations.count, 1); // one and only one annotation is active
    XCTAssertEqual(_vcUnderTest.confirmButton.enabled, false); // no place is selected yet and so it can't continue to the next step
    XCTAssertEqualObjects(_vcUnderTest.lastAnnotation, [_vcUnderTest.mapView.annotations lastObject]); // lastAnnotation is set to the only annotation present
    XCTAssertEqual(_vcUnderTest.lastAnnotation.title, NSLocalizedString(@"MapVC:Location:PinWaitTitle", nil)); // annotation should be hanged in waiting state
    XCTAssertEqualWithAccuracy(_vcUnderTest.mapView.centerCoordinate.latitude, _vcUnderTest.lastAnnotation.coordinate.latitude, 1.0); // annotation should match location
    XCTAssertEqualWithAccuracy(_vcUnderTest.mapView.centerCoordinate.longitude, _vcUnderTest.lastAnnotation.coordinate.longitude, 1.0); // annotation should match location
    XCTAssertEqualWithAccuracy(_vcUnderTest.lastAnnotation.coordinate.latitude,  40.0, 1.0); // annotation should match location
    XCTAssertEqualWithAccuracy(_vcUnderTest.lastAnnotation.coordinate.longitude,  50.0, 1.0); // annotation should match location

    
}
- (void)testGeocoderFails {
    CLGeocoderMock *geo = [CLGeocoderMock new];
    geo.returnedError = [[NSError alloc] init];
    geo.returnedLocations = nil;
    _vcUnderTest.geocoder = geo;
    
    [_vcUnderTest moveCurrentPositionToLocation:CLLocationCoordinate2DMake(45.0, 45.0)];
    
    XCTAssertEqual(_vcUnderTest.mapView.annotations.count, 0); // there should not be any annotation
    XCTAssertEqual(_vcUnderTest.confirmButton.enabled, false); // no place is selected and so it can't continue to the next step
    XCTAssertEqual(_vcUnderTest.lastAnnotation, nil); // state should reflect that there should not be any annotation
}
- (void)testLocatorHangs {
    CLGeocoderMock *geo = [CLGeocoderMock new];
    geo.returnedError = nil;
    id placemark = OCMClassMock([CLPlacemark class]);
    OCMStub([placemark locality]).andReturn(@"city");
    geo.returnedLocations = @[placemark];
    _vcUnderTest.geocoder = geo;
    
    id mock = OCMClassMock([CLLocationManager class]);
    _vcUnderTest.locator = mock;
    
    CLLocation* location = [[CLLocation alloc] initWithLatitude:40.0 longitude:50.0];
    NSArray* locations = @[location];
    
    [_vcUnderTest locatorPressed];
    
    XCTAssertEqual(_vcUnderTest.confirmButton.enabled, false);
    XCTAssertNotEqual(_vcUnderTest.alertController, nil);
    XCTAssertEqual(_vcUnderTest.alertController.message, NSLocalizedString(@"MapVC:Locating:Message", @""));
}
- (void)testLocatorSucceed {
    CLGeocoderMock *geo = [CLGeocoderMock new];
    geo.returnedError = nil;
    id placemark = OCMClassMock([CLPlacemark class]);
    OCMStub([placemark locality]).andReturn(@"city");
    geo.returnedLocations = @[placemark];
    _vcUnderTest.geocoder = geo;
    
    id mock = OCMClassMock([CLLocationManager class]);
    _vcUnderTest.locator = mock;
    
    CLLocation* location = [[CLLocation alloc] initWithLatitude:40.0 longitude:50.0];
    NSArray* locations = @[location];
    
    [_vcUnderTest locatorPressed];
    [_vcUnderTest locationManager:mock didUpdateLocations:locations];
    
    XCTAssertEqual(_vcUnderTest.mapView.annotations.count, 1); // one and only one annotation is active
    XCTAssertEqual(_vcUnderTest.confirmButton.enabled, true); // a place is selected and app can continue to next
    XCTAssertEqualObjects(_vcUnderTest.lastAnnotation, [_vcUnderTest.mapView.annotations lastObject]); // lastAnnotation is set to the only annotation present
    XCTAssertEqual(_vcUnderTest.lastAnnotation.title, NSLocalizedString(@"city", nil)); // annotation should be hanged in waiting state
    XCTAssertEqualWithAccuracy(_vcUnderTest.mapView.centerCoordinate.latitude, _vcUnderTest.lastAnnotation.coordinate.latitude, 1.0); // center should match location
    XCTAssertEqualWithAccuracy(_vcUnderTest.mapView.centerCoordinate.longitude, _vcUnderTest.lastAnnotation.coordinate.longitude, 1.0); // center should match location
    XCTAssertEqualWithAccuracy(_vcUnderTest.lastAnnotation.coordinate.latitude,  40.0, 1.0); // annotation should match location
    XCTAssertEqualWithAccuracy(_vcUnderTest.lastAnnotation.coordinate.longitude,  50.0, 1.0); // annotation should match location
}
- (void)testLocatorFails {
    CLGeocoderMock *geo = [CLGeocoderMock new];
    geo.returnedError = nil;
    id placemark = OCMClassMock([CLPlacemark class]);
    OCMStub([placemark locality]).andReturn(@"city");
    geo.returnedLocations = @[placemark];
    _vcUnderTest.geocoder = geo;
    
    id mock = OCMClassMock([CLLocationManager class]);
    _vcUnderTest.locator = mock;
    
    CLLocation* location = [[CLLocation alloc] initWithLatitude:40.0 longitude:50.0];
    
    [_vcUnderTest locatorPressed];
    [_vcUnderTest locationManager:mock didFailWithError:[NSError new]];
    
    XCTAssertEqual(_vcUnderTest.confirmButton.enabled, false);
    XCTAssertNotEqual(_vcUnderTest.alertController, nil);
    XCTAssertEqual(_vcUnderTest.alertController.message, NSLocalizedString(@"MapVC:Error:Location:Message", @""));
}

/*
- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}
*/

@end
