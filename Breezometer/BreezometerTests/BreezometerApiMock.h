//
//  BreezometerApiMock.h
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "BreezometerApi.h"

@interface BreezometerApiMock : BreezometerApi

@property NSDictionary* responseObject;
@property NSError* error;

@end
