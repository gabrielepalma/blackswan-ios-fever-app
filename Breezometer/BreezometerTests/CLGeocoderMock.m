//
//  CLGeocoderMock.m
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "CLGeocoderMock.h"

@implementation CLGeocoderMock

- (void)reverseGeocodeLocation:(CLLocation *)location completionHandler:(CLGeocodeCompletionHandler)completionHandler {
    completionHandler(_returnedLocations, _returnedError);
}

@end
