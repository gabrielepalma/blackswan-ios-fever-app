//
//  BreezometerApiMock.m
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "BreezometerApiMock.h"

@implementation BreezometerApiMock

- (void)getAirQualityDataForLatitude:(float)latitude andLongitude:(float)longitude onSuccess:(void (^)(NSURLSessionDataTask *, id))successBlock onFailure:(void (^)(NSURLSessionDataTask *, NSError *))failureBlock {
    if(self.error) {
        failureBlock(nil, _error);
    }
    else {
        successBlock(nil, _responseObject);
    }
}

@end
