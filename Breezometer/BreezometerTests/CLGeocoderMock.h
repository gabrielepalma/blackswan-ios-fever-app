//
//  CLGeocoderMock.h
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <CoreLocation/CoreLocation.h>

@interface CLGeocoderMock : CLGeocoder

@property NSArray<CLPlacemark*>* returnedLocations;
@property NSError* returnedError;


@end
