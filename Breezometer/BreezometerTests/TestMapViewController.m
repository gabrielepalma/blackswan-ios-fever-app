//
//  TestMapViewController.m
//  Breezometer
//
//  Created by Gabriele Palma on 04/05/16.
//  Copyright © 2016 Gabriele Palma. All rights reserved.
//

#import "TestMapViewController.h"
#import "MapViewController.h"

@interface TestMapViewController ()

@property MapViewController* vcUnderTest;

@end

@implementation TestMapViewController

- (void)setUp {
    [super setUp];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    _vcUnderTest = [storyboard instantiateViewControllerWithIdentifier:@"mapViewController"];
    UIView *v = _vcUnderTest.view; v = v;
}

- (void)tearDown {
    _vcUnderTest = nil;
    [super tearDown];
}

- (void)testLocationFail {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}
- (void)testLocationSucceed {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}
- (void)testLocationHangs {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

-(void)testGeocoderFail {
    
}

-(void)testGeocoderSucceed {
    
}

-(void)testGeocoderHangs {
    
}

@end
