//
//  BookmarksTests.m
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <XCTest/XCTest.h>
#import "BookmarksTableViewController.h"
#import "Bookmark+CoreDataProperties.h"

@interface BookmarksTests : XCTestCase

@property BookmarksTableViewController* vcUnderTest;

@end

@implementation BookmarksTests

- (void)setUp {
    [super setUp];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _vcUnderTest = [storyboard instantiateViewControllerWithIdentifier:@"bookmarksTableViewController"];
    UIView* v = _vcUnderTest.view; v = v;
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"Bookmark"];
    NSError *error = nil;
    NSArray *results = [_vcUnderTest.managedObjectContext executeFetchRequest:request error:&error];
    for (NSManagedObject *obj in results) {
        [_vcUnderTest.managedObjectContext deleteObject:obj];
    }
    [_vcUnderTest.managedObjectContext save:nil];
}

- (void)tearDown {
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"Bookmark"];
    NSError *error = nil;
    NSArray *results = [_vcUnderTest.managedObjectContext executeFetchRequest:request error:&error];
    for (NSManagedObject *obj in results) {
        [_vcUnderTest.managedObjectContext deleteObject:obj];
    }
    [_vcUnderTest.managedObjectContext save:nil];
    
    [super tearDown];
}

- (void)testZeroToOne {
    XCTAssertEqual([_vcUnderTest numberOfSectionsInTableView:_vcUnderTest.tableView], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView numberOfRowsInSection:1], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]].textLabel.text, NSLocalizedString(@"BookmarkVC:Cell:EmptyTable", nil));
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Bookmark" inManagedObjectContext:_vcUnderTest.managedObjectContext];
    Bookmark* bookmark = [[Bookmark alloc] initWithEntity:entity insertIntoManagedObjectContext:_vcUnderTest.managedObjectContext];
    bookmark.latitude = [NSNumber numberWithFloat:40.0];
    bookmark.longitude = [NSNumber numberWithFloat:50.0];
    bookmark.city = @"city";
    
    [_vcUnderTest.managedObjectContext insertObject:bookmark];
    [_vcUnderTest.managedObjectContext save:nil];
    
    XCTAssertEqual([_vcUnderTest numberOfSectionsInTableView:_vcUnderTest.tableView], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView numberOfRowsInSection:1], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]].textLabel.text, @"city");
}
- (void)testOneToZero {
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Bookmark" inManagedObjectContext:_vcUnderTest.managedObjectContext];
    Bookmark* bookmark = [[Bookmark alloc] initWithEntity:entity insertIntoManagedObjectContext:_vcUnderTest.managedObjectContext];
    bookmark.latitude = [NSNumber numberWithFloat:40.0];
    bookmark.longitude = [NSNumber numberWithFloat:50.0];
    bookmark.city = @"city";
    
    [_vcUnderTest.managedObjectContext insertObject:bookmark];
    [_vcUnderTest.managedObjectContext save:nil];
    
    XCTAssertEqual([_vcUnderTest numberOfSectionsInTableView:_vcUnderTest.tableView], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView numberOfRowsInSection:1], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]].textLabel.text, @"city");
    
    [_vcUnderTest.managedObjectContext deleteObject:bookmark];
    [_vcUnderTest.managedObjectContext save:nil];
    
    XCTAssertEqual([_vcUnderTest numberOfSectionsInTableView:_vcUnderTest.tableView], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView numberOfRowsInSection:1], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]].textLabel.text, NSLocalizedString(@"BookmarkVC:Cell:EmptyTable", nil));
}
- (void)testOneToTwo {
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Bookmark" inManagedObjectContext:_vcUnderTest.managedObjectContext];
    Bookmark* bookmark1 = [[Bookmark alloc] initWithEntity:entity insertIntoManagedObjectContext:_vcUnderTest.managedObjectContext];
    bookmark1.latitude = [NSNumber numberWithFloat:40.0];
    bookmark1.longitude = [NSNumber numberWithFloat:50.0];
    bookmark1.city = @"city1";
    
    [_vcUnderTest.managedObjectContext insertObject:bookmark1];
    [_vcUnderTest.managedObjectContext save:nil];
    
    XCTAssertEqual([_vcUnderTest numberOfSectionsInTableView:_vcUnderTest.tableView], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView numberOfRowsInSection:1], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]].textLabel.text, @"city1");
    
    Bookmark* bookmark2 = [[Bookmark alloc] initWithEntity:entity insertIntoManagedObjectContext:_vcUnderTest.managedObjectContext];
    bookmark2.latitude = [NSNumber numberWithFloat:40.0];
    bookmark2.longitude = [NSNumber numberWithFloat:50.0];
    bookmark2.city = @"city2";
    
    [_vcUnderTest.managedObjectContext insertObject:bookmark2];
    [_vcUnderTest.managedObjectContext save:nil];
    
    XCTAssertEqual([_vcUnderTest numberOfSectionsInTableView:_vcUnderTest.tableView], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView numberOfRowsInSection:1], 2);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]].textLabel.text, @"city2");
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]].textLabel.text, @"city1");
    
}
- (void)testTwoToOne {
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Bookmark" inManagedObjectContext:_vcUnderTest.managedObjectContext];
    Bookmark* bookmark1 = [[Bookmark alloc] initWithEntity:entity insertIntoManagedObjectContext:_vcUnderTest.managedObjectContext];
    bookmark1.latitude = [NSNumber numberWithFloat:40.0];
    bookmark1.longitude = [NSNumber numberWithFloat:50.0];
    bookmark1.city = @"city1";
    [_vcUnderTest.managedObjectContext insertObject:bookmark1];
    
    Bookmark* bookmark2 = [[Bookmark alloc] initWithEntity:entity insertIntoManagedObjectContext:_vcUnderTest.managedObjectContext];
    bookmark2.latitude = [NSNumber numberWithFloat:40.0];
    bookmark2.longitude = [NSNumber numberWithFloat:50.0];
    bookmark2.city = @"city2";
    [_vcUnderTest.managedObjectContext insertObject:bookmark2];
    [_vcUnderTest.managedObjectContext save:nil];
    
    XCTAssertEqual([_vcUnderTest numberOfSectionsInTableView:_vcUnderTest.tableView], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView numberOfRowsInSection:1], 2);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]].textLabel.text, @"city2");
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:1 inSection:0]].textLabel.text, @"city1");
    

    [_vcUnderTest.managedObjectContext deleteObject:bookmark1];
    [_vcUnderTest.managedObjectContext save:nil];
    
    XCTAssertEqual([_vcUnderTest numberOfSectionsInTableView:_vcUnderTest.tableView], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView numberOfRowsInSection:1], 1);
    XCTAssertEqual([_vcUnderTest tableView:_vcUnderTest.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]].textLabel.text, @"city2");
}

@end
