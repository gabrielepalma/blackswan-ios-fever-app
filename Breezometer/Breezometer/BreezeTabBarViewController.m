//
//  BreezeTabBarViewController.m
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "BreezeTabBarViewController.h"
#import "UIColor+ThemeHelper.h"

@interface BreezeTabBarViewController ()

@end

@implementation BreezeTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tabBar setTintColor:[UIColor breezometerPrimaryColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
