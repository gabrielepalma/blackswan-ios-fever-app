//
//  BreezometerApi.m
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "BreezometerApi.h"
#import <AFHTTPSessionManager.h>

@implementation BreezometerApi

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.apiUrl = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"BreezometerBaseURL"];
        self.apiKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"BreezometerApiKey"];
    }
    return self;
}

- (void)getAirQualityDataForLatitude:(float)latitude andLongitude:(float)longitude
                           onSuccess:(void (^)(NSURLSessionDataTask * task, id responseObject))successBlock
                           onFailure:(void (^)(NSURLSessionDataTask * task, NSError * error))failureBlock {
   
    NSURL *baseURL = [NSURL URLWithString:self.apiUrl];
    NSDictionary *parameters = @{@"lat": [NSNumber numberWithFloat:latitude], @"lon": [NSNumber numberWithFloat:longitude],  @"key": self.apiKey };
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:@"baqi/" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@", responseObject);
        successBlock(task, responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
        failureBlock(task, error);
    }];
}

@end