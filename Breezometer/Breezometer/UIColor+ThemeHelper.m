//
//  UIColor+ThemeHelper.m
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "UIColor+ThemeHelper.h"

// Theme support is provided in plist file and this is a convenience method to retrieve color information
@implementation UIColor (ThemeHelper)

+ (UIColor *) breezometerPrimaryColor {
    NSDictionary* theme = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"BreezometerTheme"];
    NSString* strColor = [theme objectForKey:@"colorPrimaryColor"];
    NSScanner* scanner = [NSScanner scannerWithString:strColor];
    scanner.scanLocation = 1;
    unsigned int rgbColor = 0;
    [scanner scanHexInt:&rgbColor];
    return [UIColor convertHexToColor:rgbColor];
}
+ (UIColor *) breezometerBackground {
    NSDictionary* theme = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"BreezometerTheme"];
    NSString* strColor = [theme objectForKey:@"colorBackground"];
    NSScanner* scanner = [NSScanner scannerWithString:strColor];
    scanner.scanLocation = 1;
    unsigned int rgbColor = 0;
    [scanner scanHexInt:&rgbColor];
    return [UIColor convertHexToColor:rgbColor];
}
+ (UIColor *) breezometerPrimaryContrast {
    NSDictionary* theme = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"BreezometerTheme"];
    NSString* strColor = [theme objectForKey:@"colorPrimaryContrast"];
    NSScanner* scanner = [NSScanner scannerWithString:strColor];
    scanner.scanLocation = 1;
    unsigned int rgbColor = 0;
    [scanner scanHexInt:&rgbColor];
    return [UIColor convertHexToColor:rgbColor];
}

+ (UIColor*) convertHexToColor:(unsigned int)hex {
    return [UIColor colorWithRed:((hex & 0xFF0000) >> 16)/255.0 green:((hex & 0xFF00) >> 8)/255.0 blue:(hex & 0xFF)/255.0 alpha:1.0];
}

@end
