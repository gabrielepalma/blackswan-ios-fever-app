//
//  Bookmark+CoreDataProperties.h
//  
//
//  Created by Gabriele Palma on 02/05/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Bookmark.h"

NS_ASSUME_NONNULL_BEGIN

@interface Bookmark (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *city;
@property (nullable, nonatomic, retain) NSNumber *latitude;
@property (nullable, nonatomic, retain) NSNumber *longitude;

@end

NS_ASSUME_NONNULL_END
