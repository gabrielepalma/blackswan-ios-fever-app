//
//  Bookmark+CoreDataProperties.m
//  
//
//  Created by Gabriele Palma on 02/05/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Bookmark+CoreDataProperties.h"

@implementation Bookmark (CoreDataProperties)

@dynamic city;
@dynamic latitude;
@dynamic longitude;

@end
