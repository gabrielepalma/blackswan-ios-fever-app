//
//  BreezometerApi.h
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <Foundation/Foundation.h>

@interface BreezometerApi : NSObject


@property NSString* apiKey;
@property NSString* apiUrl;

- (void)getAirQualityDataForLatitude:(float)latitude andLongitude:(float)longitude
                           onSuccess:(void (^)(NSURLSessionDataTask * task, id responseObject))successBlock
                           onFailure:(void (^)(NSURLSessionDataTask * task, NSError * error))failureBlock;

@end
