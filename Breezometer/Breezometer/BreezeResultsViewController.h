//
//  BreezeResultsViewController.h
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <UIKit/UIKit.h>
#import "Bookmark+CoreDataProperties.h"
#import "BreezeDataModel.h"
#import "BreezometerApi.h"
#import "AppDelegate.h"
#import "EXTScope.h"
#import "UIColor+ThemeHelper.h"

@interface BreezeResultsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong) Bookmark* bookmark;
@property float latitude;
@property float longitude;
@property (strong) NSString* city;
@property (strong) BreezeDataModel* dataModel;
@property (strong) BreezometerApi* apiHelper;

@property (weak) IBOutlet UITableView* tableView;
@property (weak) IBOutlet UIBarButtonItem* bookmarkButton;
@property (weak) IBOutlet UIActivityIndicatorView* spinnerIndicator;

- (void) configureLocationAtLatitude:(float)latitude andLongitude:(float)longitude andCity:(NSString *)city;
- (void) configureLocationForBookmark:(Bookmark*)bookmark;

- (void) viewWillAppear:(BOOL)animated;
- (void) loadInformationFromPublicApi;

- (IBAction)bookmarkTapped:(id)sender;

#pragma mark Table View delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;

@end
