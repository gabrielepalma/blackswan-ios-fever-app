//
//  BreezeDataModel.h
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <JSONModel/JSONModel.h>

@interface BreezeHints : JSONModel

@property (strong) NSString* children;
@property (strong) NSString* sport;
@property (strong) NSString* health;
@property (strong) NSString* inside;
@property (strong) NSString* outside;

@end

@interface BreezePollutantInfo : JSONModel

@property (strong) NSString* main;
@property (strong) NSString* effects;
@property (strong) NSString* causes;

@end


@interface BreezeDataModel : JSONModel

@property int breezometer_aqi;
@property (strong) NSString* breezometer_description;
@property (strong) BreezeHints* random_recommendations;
@property (strong) NSString* dominant_pollutant_description;
@property (strong) BreezePollutantInfo* dominant_pollutant_text;

@end
