//
//  MapViewController.m
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "MapViewController.h"
#import "BreezeResultsViewController.h"
#import "EXTScope.h"
#import "UIColor+ThemeHelper.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        UITabBarItem* mapItem = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemSearch tag:0];
        [self setTabBarItem:mapItem];
        
        self.geocoder = [CLGeocoder new];
        self.locator = [CLLocationManager new];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTheme];

    [self.locator setDelegate:self];
    [self.locator setPausesLocationUpdatesAutomatically:NO];
    [self.locator setDesiredAccuracy:kCLLocationAccuracyKilometer];
    
    CLLocationCoordinate2D startCoord = CLLocationCoordinate2DMake(51.5085300, -0.1257400);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(startCoord, 800, 800)];
    adjustedRegion.span.longitudeDelta  = 0.02;
    adjustedRegion.span.latitudeDelta  = 0.02;
    [self.mapView setRegion:adjustedRegion animated:YES];
    self.confirmButton.enabled = false;
    [self moveCurrentPositionToLocation:startCoord];
}

- (void) setupTheme {
    self.view.backgroundColor = [UIColor breezometerBackground];
    self.confirmButton.backgroundColor = [UIColor breezometerPrimaryColor];
    [self.confirmButton setTitleColor:[UIColor breezometerPrimaryContrast] forState:UIControlStateNormal];
    self.confirmButton.layer.cornerRadius = 15.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) locatorPressed {
    [self.locator requestWhenInUseAuthorization];

    self.confirmButton.enabled = false;
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"MapVC:Locating:Title", @"")
                                  message:NSLocalizedString(@"MapVC:Locating:Message", @"")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    @weakify(self);
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"MapVC:Locating:Cancel", @"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [self.locator stopUpdatingLocation];
    }];
    [alert addAction:cancel];
    
    self.alertController = alert;
    [self presentViewController:alert animated:NO completion:nil];
    
    [self.locator startUpdatingLocation];
}

- (IBAction) handleLongPress:(UILongPressGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateBegan) {
        self.confirmButton.enabled = false;
        CGPoint point = [sender locationInView:self.mapView];
        CLLocationCoordinate2D locCoord = [self.mapView convertPoint:point toCoordinateFromView:self.mapView];
        [self moveCurrentPositionToLocation:locCoord];
    }
}

#pragma mark - Navigation

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    BreezeResultsViewController* next = (BreezeResultsViewController*)[segue destinationViewController];
    [next configureLocationAtLatitude:_latitude  andLongitude:_longitude andCity:_city];
}

#pragma mark - Location

// Center the map, add a pin, then invoke reverse geocoding to determine the city
- (void) moveCurrentPositionToLocation:(CLLocationCoordinate2D)location {
    [self.mapView setCenterCoordinate:location];
    [self setPinAtLocation:location];
    
    
    @weakify(self);
    [self.geocoder reverseGeocodeLocation:[[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude] completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        @strongify(self)
        if(error || placemarks.count == 0){
            if(self.lastAnnotation){
                [self.mapView removeAnnotation:self.lastAnnotation];
            }
            self.lastAnnotation = nil;
            [self displayBriefLocationErrorMessageWithText:NSLocalizedString(@"MapVC:Error:Geocoding:Message", @"")];
        }
        else {
            self.latitude = location.latitude;
            self.longitude = location.longitude;
            self.city = [[placemarks lastObject] locality];
            [self.lastAnnotation setTitle:self.city];
            self.confirmButton.enabled = true;
        }
    }];
}

// Set the pin at given coordinate and select it
- (void) setPinAtLocation:(CLLocationCoordinate2D)location {
    if(self.lastAnnotation){
        [self.mapView removeAnnotation:self.lastAnnotation];
    }
    self.lastAnnotation = [[MKPointAnnotation alloc] init];
    [self.lastAnnotation setCoordinate:location];
    [self.lastAnnotation setTitle:NSLocalizedString(@"MapVC:Location:PinWaitTitle", @"")];
    [self.mapView addAnnotation:self.lastAnnotation];
    [self.mapView selectAnnotation:self.lastAnnotation animated:NO];
}

// Location failed, send a message of error
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [self.alertController dismissViewControllerAnimated:NO completion:nil];
    [self displayBriefLocationErrorMessageWithText:NSLocalizedString(@"MapVC:Error:Location:Message", @"")];
}

// Location succeeded: move there, add pin and reverse geocode the location
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *currentLocation = locations.lastObject;
    NSTimeInterval howRecent = [currentLocation.timestamp timeIntervalSinceNow];
    if (fabs(howRecent) < 15.0) {
        [self.locator stopUpdatingLocation];
        [self.alertController dismissViewControllerAnimated:NO completion:nil];
        [self moveCurrentPositionToLocation:currentLocation.coordinate];
    }
}

// Convenience method to display a message
- (void) displayBriefLocationErrorMessageWithText:(NSString*)text {
    self.alertController = [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"MapVC:Error:Title", @"")
                                  message:text
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"MapVC:Error:Ok", @"") style:UIAlertActionStyleCancel handler:nil];
    [self.alertController addAction:cancel];
    
    @weakify(self);
    [self presentViewController:_alertController animated:NO completion:^{
        @strongify(self);
        self.alertController = nil;
    }];
}

@end
