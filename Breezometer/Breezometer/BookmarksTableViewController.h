//
//  FavouritesTableViewController.h
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <UIKit/UIKit.h>
@import CoreData;

@interface BookmarksTableViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController;

@end
