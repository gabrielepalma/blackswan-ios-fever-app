//
//  BreezeResultsViewController.m
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import "BreezeResultsViewController.h"


@interface BreezeResultsViewController ()

@end

@implementation BreezeResultsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.estimatedRowHeight = 44.0;;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"standardCell"];
    self.spinnerIndicator.tintColor = [UIColor breezometerPrimaryColor];
    self.apiHelper = [BreezometerApi new];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setBarButtonImage];
    [self loadInformationFromPublicApi];
}

// Query the api to get air quality information
- (void) loadInformationFromPublicApi {
    self.spinnerIndicator.hidden = false;
    self.tableView.hidden = true;
    [self.spinnerIndicator startAnimating];
    @weakify(self);
    [self.apiHelper getAirQualityDataForLatitude:self.latitude andLongitude:self.longitude onSuccess:^(NSURLSessionDataTask *task, id responseObject) {
        @strongify(self);
        self.spinnerIndicator.hidden = true;
        self.tableView.hidden = false;
        [self.spinnerIndicator stopAnimating];
        self.dataModel = [[BreezeDataModel alloc] initWithDictionary:responseObject  error:nil];
        [self.tableView reloadData];
    } onFailure:^(NSURLSessionDataTask *task, NSError *error) {
        @strongify(self);
        self.spinnerIndicator.hidden = true;
        self.tableView.hidden = true;
        [self.spinnerIndicator stopAnimating];
        [self showErrorMessage];
    }];
}

- (void) showErrorMessage {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:NSLocalizedString(@"BreezeVC:Error:Title", @"")
                                  message:NSLocalizedString(@"BreezeVC:Error:Message", @"")
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    
    {
        @weakify(self);
        UIAlertAction* ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"BreezeVC:Error:Retry", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            @strongify(self);
            [self loadInformationFromPublicApi];
        }];
        [alert addAction:ok];
    }
    
    {
        @weakify(self);
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"BreezeVC:Error:Cancel", @"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            @strongify(self);
            [[self navigationController] popViewControllerAnimated:true];
        }];
        [alert addAction:cancel];
    }
    
    [self presentViewController:alert animated:NO completion:nil];
}

// Either add or remove the bookmark
- (IBAction)bookmarkTapped:(id)sender {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    if(self.bookmark != nil){
        [context deleteObject:self.bookmark];
        self.bookmark = nil;
    }
    else {
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Bookmark" inManagedObjectContext:context];
        Bookmark* bookmark = [[Bookmark alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
        bookmark.latitude = [NSNumber numberWithFloat:self.latitude];
        bookmark.longitude = [NSNumber numberWithFloat:self.longitude];
        bookmark.city = self.city;
        self.bookmark = bookmark;
    }
    [context save:nil];
    [self setBarButtonImage];
}

// Convenience method to update the bookmark button image
- (void) setBarButtonImage {
    if(self.bookmark != nil){
        self.bookmarkButton.image = [UIImage imageNamed:@"FullStar"];
    }
    else {
        self.bookmarkButton.image = [UIImage imageNamed:@"EmptyStar"];
    }
}

#pragma mark Table View delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(!self.dataModel) return 1;
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier:@"standardCell" forIndexPath:indexPath];
    cell.textLabel.numberOfLines = 0;
    if(indexPath.section == 0){
        switch (indexPath.row) {
            case 0:
                if(!self.dataModel) {
                    cell.textLabel.text = NSLocalizedString(@"BreezeResultsVC:EmptySet", @"");
                }
                else {
                    cell.textLabel.text = self.city;
                }
                break;
            case 1:
                cell.textLabel.text = [NSString stringWithFormat:@"%d", self.dataModel.breezometer_aqi];
                break;
            case 2:
                cell.textLabel.text = self.dataModel.breezometer_description ;
                break;
            default:
                break;
        }
    }
    else if(indexPath.section == 1){
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = self.dataModel.random_recommendations.children;
                break;
            case 1:
                cell.textLabel.text = self.dataModel.random_recommendations.health;
                break;
            case 2:
                cell.textLabel.text = self.dataModel.random_recommendations.inside;
                break;
            case 3:
                cell.textLabel.text = self.dataModel.random_recommendations.outside;
                break;
            case 4:
                cell.textLabel.text = self.dataModel.random_recommendations.sport;
                break;
            default:
                break;
        }
        
    }
    else if(indexPath.section == 2){
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = self.dataModel.dominant_pollutant_description;
                break;
            case 1:
                cell.textLabel.text = self.dataModel.dominant_pollutant_text.main;
                break;
            case 2:
                cell.textLabel.text = self.dataModel.dominant_pollutant_text.effects;
                break;
            case 3:
                cell.textLabel.text = self.dataModel.dominant_pollutant_text.causes;
                break;
            default:
                break;
        }
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 45.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section==0){
        if(!self.dataModel) return 1;
        return 3;
    }
    else if(section==1) return 5;
    else return 4;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView* view = [UIView new];
    UILabel* lbl = [UILabel new];
    [view addSubview:(lbl)];
    [lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    [view addConstraint:[NSLayoutConstraint constraintWithItem:lbl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    [view addConstraint:[NSLayoutConstraint constraintWithItem:lbl attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    lbl.textColor = [UIColor breezometerPrimaryContrast];
    lbl.backgroundColor = [UIColor breezometerPrimaryColor];
    
    if(section == 0){
        lbl.text = NSLocalizedString(@"BreezeVC:Section:Info", nil);
    }
    else if(section == 1){
        lbl.text = NSLocalizedString(@"BreezeVC:Section:Hints", nil);
    }
    else if(section == 2){
        lbl.text = NSLocalizedString(@"BreezeVC:Section:Pollutant", nil);
    }
    
    lbl.layer.cornerRadius = 10;
    lbl.layer.masksToBounds = true;
    
    return view;
}

#pragma mark Segue configuration methods

// Configuration with location
- (void)configureLocationAtLatitude:(float)latitude andLongitude:(float)longitude andCity:(NSString *)city {
    _bookmark = nil;
    _latitude = latitude;
    _longitude = longitude;
    _city = city;
}

// Configuration from bookmark
- (void)configureLocationForBookmark:(Bookmark *)bookmark {
    _bookmark = bookmark;
    _latitude = bookmark.latitude.floatValue;
    _longitude = bookmark.longitude.floatValue;
    _city = bookmark.city;
}


@end

