//
//  UIColor+ThemeHelper.h
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface UIColor (ThemeHelper)

+ (UIColor *) breezometerPrimaryColor;
+ (UIColor *) breezometerBackground;
+ (UIColor *) breezometerPrimaryContrast;
+ (UIColor *) convertHexToColor:(unsigned int)hex;

@end
