//
//  MapViewController.h
//  Breezometer
//
//  ┌─┐┌─┐   Copyright (c) 2016
//  │ ┬├─┘   Gabriele Palma
//  └─┘┴     www.gabrielepalma.it
//

#import <UIKit/UIKit.h>
@import MapKit;

@interface MapViewController : UIViewController <CLLocationManagerDelegate, UIGestureRecognizerDelegate>

@property (weak) IBOutlet MKMapView* mapView;
@property (weak) IBOutlet UIButton* confirmButton;

// currently selected values
@property float latitude;
@property float longitude;
@property NSString* city;

@property (strong) CLGeocoder* geocoder;
@property (strong) id locator;
@property (strong) UIAlertController* alertController; // messages
@property (strong) MKPointAnnotation* lastAnnotation;
@property (strong) UILongPressGestureRecognizer* gestureRecognizer; // long press to change pin and selected coordinates

- (IBAction) locatorPressed;
- (IBAction) handleLongPress:(UILongPressGestureRecognizer *)sender;

#pragma mark - Navigation

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

#pragma mark - Location

- (void) moveCurrentPositionToLocation:(CLLocationCoordinate2D)location;
- (void) setPinAtLocation:(CLLocationCoordinate2D)location;
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error;
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations;
- (void) displayBriefLocationErrorMessageWithText:(NSString*)text;

@end
